# Nuestro primer programa

Tradicionalmente, el primer programa que escribimos en cualquier lenguaje de programación es el llamado "Hola mundo". Es un programa que imprime el texto "Hola mundo" en un dispositivo de visualización, en la mayoria de los cosas una pantalla de monitor. Se caracteriza por su sencillez y se le considera fundamental desde el punto de vista didáctico.

En esta ocasión utilizaremos la linea de comandos para compilar nuestros programas.


## Escribiendo el programa

En nuestro editor de texto favorito, creamos un nuevo archivo y escribimos lo siguiente:

```go
package main
import "fmt"

//Esto es un comentario
func main() {
    fmt.Println("Hola mundo")
}
```

Guardaremos el archivocon una extención .go, ejemplo "hola-mundo.go". 

## Compilando un programa en Go

Una vez escrito y guardado nustro programa, abriremos una terminal dentro de la carpeta donde se encuentre el archivo .go y escribiremos el sigueitne comando:

```linux
$ go run helloWorld.go

```
Una vez compilado y ejecutado el programa se observara en la terminal el mensaje "Hola mundo":
![](../Imagenes/hola-mundo.gif)

## Como leer un programa escrito en Go

Los programas en Go se leen de arriba hacia abajo y de izquierda a derecha (como un libro). En nuestro primer programa la primera linea es:
```go
package main
```
De esta manera se declara un paquete y todos los programas en Go deben empezar con un paquete, si nuestra aplicación va a ejecutarse por sí misma el paquete debe ser main.

```go
import "fmt"
```
La palabra "import" es utilizada para incluir el codigo de otro paquete para poder utilizarlo en nuestro programa. El paquete "fmt" implementa funciones para las entradas y salidas de nuestros programas (ejemplo, imprimir en pantalla y pedir datos al usuario).
La siguiente linea comienza con "//" lo que indica que lo escrito a su lado derecho es un comentario.

Siguiendo con nuestro programa tenemos la declaración de la funcion de nuestro programa:
```go
func main() {
   fmt.Println("Hello, World)
}
```
Las funciones son los componentes básicos de un programa en Go. Tienen entradas, salidas y una serie de pasos llamados instrucciones que se ejecutan en orden. Todas las funciones comienzan con la palabra clave func seguida del nombre de la función (main, en este caso), una lista de cero o más parametros entre parentesis, un tipo de retorno que es opcional y un cuerpo entre llaves. Esta funcion no tiene parámetros, no devuelve nada y solo tiene una declaración. El nombre principal es especial porque es la función a la que se llama cuando se ejecuta el programa.

La ultima pieza de nuestro programa es la linea:
```go
fmt.Println("Hello, World")
```
En esta linea  encontramos tres componentes.
Primero, accedemos a una funcion dentro del paquete "fmt" llamado Println (fmt.Println); Luego creamos una nueva cadena que contiene "Hello, World". Con esto mandamos llamar la impresion de una cadena que se encuentra entre los parentesis despues de la funcion "Println".



#### Referencias
* http://goconejemplos.com/hola-mundo
***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).
