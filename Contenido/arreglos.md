# Arreglos

Como muchos otros lenguajes de programación, en Go también es posible trabajar con arreglos. Un arreglo es una estructura de datos que permite almacenar una colección de elementos del mismo tipo.

Los arreglos son sumamente útiles cuando necesitamos almacenar información de un mismo tipo de dato bajo un mismo identificador. Como ya se dijo, los arreglos almacenan colecciones de datos, pero suelen ser más sencillos de conceptualizar como una variable que almacena otras variables de un sólo tipo.

Los arreglos trabajan con direcciones de memoria contiguas; para acceder a uno de los elementos, regularmente, nos basta con utilizar el identificador y el número de elemento al que deseamos acceder encerrado entre corchetes.

## Declarar arreglos en Go

Para declarar arreglos en Go se necesita conocer la cantidad de miembros de la colección y su tipo de dato. El formato de declaración es el siguiente:

```go
var nombre_de_variable [TAMAÑO]tipo_de_dato
```
El nombre del arreglo puede ser cualquier identificador válido de Go, el tamaño cualquier número entero mayor a 0 y el tipo de dato cualquiera válido de Go. Supongamos que deseamos almacenar los días de la semana en un arreglo de string llamado dias_semana:

```go
var dias_semana [7] string
```
Aquí estamos creando un arreglo a que contendrá exactamente 7 strings. El tipo de los elementos y el tamaño son parte del tipo del arreglo. Por defecto un arreglo es de valor cero, lo que para los valores de tipo string significa "".

## Inicializar un arreglo en Go
Los arreglos pueden ser inicializados elemento por elemento con sentencias de asignación:

```go
var dias_semana [0] = "domingo"
var dias_semana [1] = "lunes"
var dias_semana [2] = "martes"
var dias_semana [3] = "miércoles"
var dias_semana [4] = "jueves"
var dias_semana [5] = "viernes"
var dias_semana [6] = "sábado"
```
El número que se utiliza entre corchetes es conocido como índice y es el que nos proporciona el acceso a las posiciones del arreglo con mucha facilidad. Los arreglos siempre inician a partir del índice 0, y por ende, el último índice es el tamaño del arreglo menos 1.
También se pueden inicializar al momento de la declaración:

```go
var dias_semana = [] string{"domingo","lunes",
"martes", "miércoles", "jueves", "viernes","sábado"}
```
Cuando inicializamos el arreglo durante la declaración su tamaño es definido por la cantidad de elementos que se le asignen. Para este ejemplo tenemos un arreglo de strings llamado dias_semana con un tamaño de 7.

## Acceder a elementos de un arreglo
Para acceder a un elemento del arreglo se utiliza el índice de dicho elemento:

```go
//Se accede al primer elemento del arreglo
fmt.Println(dias_semana[0])
//Se accede al segundo elemento del arreglo
fmt.Println(dias_semana[1])
...
//Se accede al quinto elemento del arreglo
fmt.Println(dias:semana[4])
...
// Se accede al séptimo y último elemento del arreglo
fmt.Println(dias_semana[6]) 
```
Los elementos del arreglo pueden ser modificados con la misma mecánica:

```go
fmt.Println(dias_semana[0])
dias_semana[0] = "día de descanso"
fmt.Println(dias_semana[0])
```
Si hacemos un programa con los ultimos 3 codigos el resultado es el siguiente:

![](../Imagenes/arreglo1.png)

## Arreglos multidimensionales
Los arreglos de los que hablamos hasta ahora han sido unidimensionales, es decir, se podía pensar en ellos como una colección en forma de lista de elementos, pero existen otra clase de arreglos a los que se les llama multidimensionales. Los arreglos multidimensionales pueden ser fácilmente comprendidos si se piensa en ellos como si fueran tablas o matrices en las que cada índice especifica una dimensión.

El formato para declarar arreglos multidimensionales es el mismo que el de los unidimensionales. Suponiendo que se desea declarar un arreglo bidimensional de 2×2:
```go
var tabla [2][2] int
```
Si se desea inicializar el arreglo en declaración puede hacerse lo siguiente:

```go
var tabla = [][]int {{1,2},{3,4}}
```
Para acceder a un elemento en específico se utilizan los índices que correspondan al elemento que necesitamos, por ejemplo:

```go
var tabla = [][]int {{1,2},{3,4}}
fmt.Println(tabla[0][1])
fmt.Println(tabla[1][0])
```
Para modificar un elemento también se utilizan los índices:

```go
tabla[0][1] = 3
tabla[1][1] = tabla[0][0] * tabla[1][0]
fmt.Println(tabla[0][1])
fmt.Println(tabla[1][1])
```
Si hacemos un programa con los ultimos 3 codigos el resultado es el siguiente:
![](../Imagenes/arreglo2.png)



***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).