package main
import "fmt"
func main() {
    
  var dias_semana = [] string{"domingo","lunes","martes", "miércoles", "jueves", "viernes","sábado"}
  //Se accede al primer elemento del arreglo
  fmt.Println(dias_semana[0])
  //Se accede al segundo elemento del arreglo
  fmt.Println(dias_semana[1])
  fmt.Println(dias_semana[2])
  fmt.Println(dias_semana[3])
  //Se accede al quinto elemento del arreglo
  fmt.Println(dias_semana[4])
  fmt.Println(dias_semana[5])
  // Se accede al séptimo y último elemento del arreglo
  fmt.Println(dias_semana[6]) 

  fmt.Println(dias_semana[0])
  dias_semana[0] = "día de descanso"
  fmt.Println(dias_semana[0])


}
 

