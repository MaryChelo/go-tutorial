package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 9
  /*Sentencia if, verifica calificación menor a 6*/
  if calificacion < 6 { /*Si la condición se cumple*/ fmt.Println("Reprobaste.") }else if calificacion >= 6 && calificacion <= 8 {
    /*Segunda condición*/
    fmt.Println("Aprobaste.")
  }else if calificacion == 9{
    /*Tercera condición*/
    fmt.Println("Aprobaste. Te fue muy bien.")
  }else{
    /*Si ninguna de las anteriores se cumplió*/
    fmt.Println("Felicidades. Aprobaste con calificación perfecta")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}

