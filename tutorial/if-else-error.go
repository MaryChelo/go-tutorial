package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 7
  /*Sentencia if, verifica calificación menor 6*/
  if calificacion < 6 {
    /*Si la condición se cumple*/
    fmt.Println("Reprobaste")
  }
  else{
    /*Si la condición no se cumple*/
    fmt.Println("Aprobaste")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}

